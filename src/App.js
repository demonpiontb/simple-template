import MainPage from './pages/MainPage';

function App() {
  return (
    <div className="app-wrapper">
      <MainPage />
    </div>
  );
}

export default App;
