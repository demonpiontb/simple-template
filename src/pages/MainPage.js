import React, { useState } from 'react';
import Sidebar from '../components/Sidebar'
import Filters from '../components/Filters'
import Header from '../components/Header';
import MainContent from '../components/MainContent';
import Box from '@mui/material/Box';
import CssBaseline from '@mui/material/CssBaseline';

export default function MainPage() {
    const [drawerWidth, setDrawerWidth] = useState(70);
    const [filtersExpanded, setFiltersExpanded] = useState(false);

    const getWidth = (width) => {
        setDrawerWidth(width);
    }

    const toggleFilters = (expanded) => {
        setFiltersExpanded(expanded)
    }

    return (
        <Box sx={{ display: 'flex' }}>
            <CssBaseline />
            <Header width={drawerWidth}/>
            <Filters toggleFilters={toggleFilters} width={drawerWidth}/>
            <Sidebar getWidth={getWidth}/>
            <MainContent filtersExpanded={filtersExpanded} />
        </Box>
    );
}