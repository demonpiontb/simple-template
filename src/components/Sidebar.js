import React, { useState } from 'react';
import Divider from '@mui/material/Divider';
import Box from '@mui/material/Box';
import Avatar from '@mui/material/Avatar';
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';
import List from '@mui/material/List';
import ListItemAvatar from '@mui/material/ListItemAvatar';
import Drawer from '@mui/material/Drawer';
import CompanyLogo from '../images/CompanyLogo.png';
import MainIcon from '../images/MainIcon.png';
import ClientsIcon from '../images/ClientsIcon.png';
import EmployeesIcon from '../images/EmployeesIcon.png';
import AnalyticsIcon from '../images/AnalyticsIcon.png';

export default function Sidebar(props) {
    const [drawerWidth, setDrawerWidth] = useState(70);
    const menuItems = [
        {'Главная': MainIcon},
        {'Клиенты': ClientsIcon},
        {'Сотрудники': EmployeesIcon},
        {'Аналитика': AnalyticsIcon},
    ];

    return (
        <Box 
            sx={{ 
                display: 'flex', 
                transition: ' all 0.2s ease', 
            }}
        >
            <Drawer
                onMouseOver={() => {setDrawerWidth(240); props.getWidth(240)}}
                onMouseOut={() => {setDrawerWidth(70); props.getWidth(70)}}
                sx={{
                    width: drawerWidth,
                    background: '#CDFFFF',
                    flexShrink: 0,
                    transition: ' all 0.2s ease',
                    '& .MuiDrawer-paper': {
                        width: drawerWidth,
                        boxSizing: 'border-box',
                        background: '#CDFFFF',
                        transition: ' all 0.2s ease'
                    },
                }}
                variant="permanent"
                anchor="left"
            >
                <List 
                    sx={{
                        background: '#80FFFF', 
                        transition: ' all 0.2s ease'
                    }}
                >
                    <ListItem 
                        key='company-name'
                        sx={{
                            height: '60px',
                            transition: ' all 0.2s ease',
                        }}
                    >
                        <ListItemAvatar>
                            <Avatar 
                                src={CompanyLogo}
                            />
                        </ListItemAvatar>
                        <ListItemText primary='Спецтех Фаворит' />
                    </ListItem>
                </List>
                <Divider />
                <List sx={{background: '#CDFFFF', transition: ' all 0.2s ease',}}>
                {menuItems.map((object) => {
                    return  <ListItem 
                        button 
                        key={Object.keys(object)[0]}
                        sx={{
                            height: '60px',
                            transition: ' all 0.2s ease',
                        }}
                    >
                    <ListItemAvatar>
                        <Avatar
                            src={Object.values(object)[0]}
                        />
                    </ListItemAvatar>
                    <ListItemText primary={Object.keys(object)[0]} />
                    </ListItem>
                })}
                </List>
            </Drawer>
        </Box>
    );
}
