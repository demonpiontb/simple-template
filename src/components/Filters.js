import React, { useState } from 'react';
import FilterIcon from '../images/FilterIcon.png';
import AppBar from '@mui/material/AppBar';
import Avatar from '@mui/material/Avatar';
import TextField from '@mui/material/TextField';
import Typography from '@mui/material/Typography';

export default function Header(props) {
    const [expanded, setExpanded] = useState(false);

    const handleToggleFilters = () => {
        props.toggleFilters(!expanded);
        setExpanded(!expanded);
    }
//REDO TO DRAWERS
    return (
        <AppBar
            position="fixed"
            sx={{ 
                width: `calc(100% - ${props.width}px)`,
                height: expanded ? '105px' : '35px',
                ml: `${props.width}px`,
                transition: ' all 0.2s ease',
                background: 'white',
                marginTop: '77px',
            }}
        >
            <Typography 
                component="div"
                sx={{
                    color: 'black', 
                    display: 'flex'
                }}
            >
                <Typography 
                    variant="h6"
                    component="div"
                    sx={{
                        marginLeft: 'auto',
                        marginRight: '15px',
                        color: 'black',
                    }}
                >
                    Фильтры
                </Typography>
                <Avatar
                onClick = {handleToggleFilters}
                src={FilterIcon}
                sx={{
                    height: '25px',
                    width: '25px',
                    margin: 'auto',
                    marginTop: '5px',
                    marginRight: '30px'
                }}
                />
            </Typography>
            {expanded ? 
            <Typography
                variant="h6"
                component="div"
                sx={{
                    marginLeft: '15px',
                    marginTop: '10px',
                    marginRight: 'auto',
                    color: 'black',
                    display: 'flex',
                    gap: '25px',
                    margin: 'auto',
                    transition: ' all 0.5s ease',
                }}
            >
                <TextField
                    id="search-input"
                    type="date"
                    variant="standard"
                    helperText="Дата от"
                />
                <TextField
                    id="search-input"
                    type="date"
                    variant="standard"
                    helperText="Дата до"
                />
                <TextField
                    id="search-input"
                    type="search"
                    variant="standard"
                    helperText="Имя клиента"
                />
                <TextField
                    id="search-input"
                    type="number"
                    variant="standard"
                    helperText="Телефон"
                />
            </Typography> 
            : 
            <></>}
        </AppBar>
    );
}