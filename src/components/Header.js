import React from 'react';
import AvatarPH from '../images/AvatarPH.png'
import AppBar from '@mui/material/AppBar';
import Avatar from '@mui/material/Avatar';
import Toolbar from '@mui/material/Toolbar';
import TextField from '@mui/material/TextField';
import Typography from '@mui/material/Typography';

export default function Header(props) {
    return (
        <AppBar
            position="fixed"
            sx={{ 
                width: `calc(100% - ${props.width}px)`, 
                ml: `${props.width}px`,
                transition: ' all 0.2s ease',
                background: '#DEDEDE',
                height: '77px',
                boxShadow: 'none',
                borderBottom: '1px solid gray'
            }}
        >
            <Toolbar sx={{height: '76px',}}>
                <Typography 
                    variant="h6"
                    component="div"
                    sx={{
                        marginRight: 'auto',
                        marginLeft: '10px',
                    }}
                >
                    <TextField
                        id="search-input"
                        label="Поиск..."
                        type="search"
                        variant="standard"
                        helperText="Введите запрос"
                    />
                </Typography>
                <Typography 
                    variant="h6"
                    component="div"
                    sx={{
                        marginLeft: 'auto',
                        marginRight: '15px',
                        color: 'black',
                    }}
                >
                    Петров В. А.
                </Typography>
                <Avatar 
                    src={AvatarPH}
                />
            </Toolbar>
        </AppBar>
    );
    }
