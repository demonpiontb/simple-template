# Simple template

## Get started
**For windows, assuming that you have git & yarn installed**

1. Open powershell and use `cd "the directory where you want to copy the app to"`
2. `git clone https://gitlab.com/demonpiontb/simple-template.git`
3. `cd ./simple-template` to move to app dir
4. `yarn` to add yarn scripts
5. `yarn start` to launch app